provider "google" {
  credentials = file( "groupsolver-thura-interview-658fbd113c33.json" )  
  project = "groupsolver-thura-interview"
  region = "us-west1"
  zone = "us-west1-c"
}

resource "google_project_service" "api" {
  for_each = toset([
    "cloudresourcemanager.googleapis.com",
    "compute.googleapis.com"
  ])
  disable_on_destroy = false
  service            = each.value
}

resource "google_compute_firewall" "web" {
  name           ="web-access"
  network        ="default"
  source_ranges  =["0.0.0.0/0"]
  allow {
    protocol ="tcp"
    ports    =["80", "443"]
  }
}

resource "google_compute_instance" "vm" {
  name = "gcp-wordpress-server"
  machine_type = "e2-medium"
    boot_disk {
    initialize_params {
      image        = "debian-cloud/debian-9"
    }
  }
  network_interface {
    network = "default" //This is to enable private IP only
    access_config {} // This is to enable public IP
  }
  metadata = {
    startup-script-url = ".Install_Apache2.sh"
  }
}

