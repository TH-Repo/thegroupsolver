#!/bin/bash -e
sudo apt update -y 
sudo curl -O https://wordpress.org/latest.tar.gz
sudo tar -zxvf latest.tar.gz
cd wordpress
cp -rf . ..
cd ..
sudo rm -Rf wordpress 
cp wp-config-sample.php wp-config.php
#create wp config
cp wp-config-sample.php wp-config.php
#set database details with perl find and replace
perl -pi -e "s/database_name_here/Wordpress_DB/g" wp-config.php
perl -pi -e "s/username_here/Chairman/g" wp-config.php
perl -pi -e "s/password_here/Apple1235/g" wp-config.php
#set WP salts
perl -i -pe'
  BEGIN {
    @chars = ("a" .. "z", "A" .. "Z", 0 .. 9);
    push @chars, split //, "!@#$%^&*()-_ []{}<>~\`+=,.;:/?|";
    sub salt { join "", map $chars[ rand @chars ], 1 .. 64 }
  }
  s/put your unique phrase here/salt()/ge
' wp-config.php

#create uploads folder and set permissions
mkdir wp-content/uploads
sudo  chmod 775 wp-content/uploads
echo "Cleaning..."
#remove zip file
sudo rm -f latest.tar.gz
#remove bash script
sudo rm -f Install_Wordpress.sh 
echo "========================="
echo "Installation is complete."
echo "========================="